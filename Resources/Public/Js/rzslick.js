(function($) {
  $(document).ready(function() {
    $("{selector}").slick({
      <f:format.raw>{slickSettings}</f:format.raw>
      <f:format.raw>{responsive}</f:format.raw>
    });
  });
})(jQuery);
