<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'RZ.' . $_EXTKEY,
    'Carousel',
    array(
        'Carousel' => 'list',

    ),
    // non-cacheable actions
    array(
        'Carousel' => '',

    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'RZ.' . $_EXTKEY,
    'Content',
    array(
        'Content' => 'list',

    ),
    // non-cacheable actions
    array(
        'Content' => '',

    )
);

if (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.0')) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rzslick/Configuration/TSconfig/ContentElementWizard76.txt">');
} else {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rzslick/Configuration/TSconfig/ContentElementWizard.txt">');
}