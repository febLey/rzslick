plugin.tx_rzslick {
	view {
		templateRootPaths {
			0 = EXT:rzslick/Resources/Private/Templates/
			1 = {$plugin.tx_rzslick.view.templateRootPath}
		}
		partialRootPaths {
			0 = EXT:rzslick/Resources/Private/Partials/
			1 = {$plugin.tx_rzslick.view.partialRootPath}
		}
		layoutRootPaths {
			0 = EXT:rzslick/Resources/Private/Layouts/
			1 = {$plugin.tx_rzslick.view.layoutRootPath}
		}
	}
	persistence {
		storagePid = {$plugin.tx_rzslick.persistence.storagePid}
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
	settings {
		addJquery = {$plugin.tx_rzslick.settings.addJquery}
    addSlick = {$plugin.tx_rzslick.settings.addSlick}
	}
}

[globalVar = LIT:1 = {$plugin.tx_rzslick.settings.addSlick}]

plugin.tx_vhs.settings.asset {
  rzslick_slick_css {
    name = rzslick_slick_css
    path = EXT:rzslick/Resources/Public/Js/slick/slick/slick.css
  }
  rzslick_slick_css_theme {
    name = rzslick_slick_css_theme
    path = EXT:rzslick/Resources/Public/Js/slick/slick/slick-theme.css
  }
  rzslick_slick_css_custom {
    name = rzslick_slick_css_custom
    path = EXT:rzslick/Resources/Public/Css/rzslick.css
  }
  rzslick_slick {
    name = rzslick_slick
    path = EXT:rzslick/Resources/Public/Js/slick/slick/slick.min.js
  }
}

[global]