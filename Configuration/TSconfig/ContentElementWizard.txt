mod.wizards.newContentElement.wizardItems.plugins {
  elements {
    rzslick_carousel {
      icon = ../typo3conf/ext/rzslick/Resources/Public/Icons/rzslick_carousel.png
      title = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_carousel_pluginWizardTitle
      description = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_carousel_pluginWizardDescription
      tt_content_defValues {
        CType = list
        list_type = rzslick_carousel
      }
    }
  }
}

mod.wizards.newContentElement.wizardItems.plugins {
  elements {
    rzslick_content {
      icon = ../typo3conf/ext/rzslick/Resources/Public/Icons/rzslick_content.png
      title = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_content_pluginWizardTitle
      description = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_content_pluginWizardDescription
      tt_content_defValues {
        CType = list
        list_type = rzslick_content
      }
    }
  }
}