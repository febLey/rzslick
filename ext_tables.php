<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Carousel',
    'LLL:EXT:rzslick/Resources/Private/Language/locallang_db.xlf:plugin_desc1'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Content',
    'LLL:EXT:rzslick/Resources/Private/Language/locallang_db.xlf:plugin_desc2'
);

$pluginSignature = str_replace('_', '', $_EXTKEY) . '_carousel';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_carousel.xml');

$pluginSignature = str_replace('_', '', $_EXTKEY) . '_content';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_content.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'slick carousel');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_rzslick_domain_model_items');

// Show tables in page module
$tables = array(
    'tx_rzslick_domain_model_items' => 'title',
);

// Traverse tables
foreach ($tables as $table => $fields) {
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables'][$table][] = array(
        'fList' => $fields,
        'icon' => true,
    );
}
