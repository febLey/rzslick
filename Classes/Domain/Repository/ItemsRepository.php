<?php
namespace RZ\Rzslick\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * The repository for Items
 */
class ItemsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     *  Find by multiple uids using, seperated string and maintain the list order
     *
     */
    public function findByUidListOrderByList($uidList)
    {
        $uidArray = explode(",", $uidList);
        $lang = $GLOBALS['TSFE']->sys_language_uid;

        if ($lang == 0) {
            $field = 'uid';
        } else {
            $field = 'l10n_parent';
        }

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching($query->in($field, $uidArray), $query->logicalAnd($query->equals('hidden', 0), $query->equals('deleted', 0), $query->logicalOr($query->equals('sys_language_uid', $lang), $query->equals('sys_language_uid', -1))));
        $query->setOrderings($this->orderByField('uid', $uidArray));

        return $query->execute();
    }

    /**
     *  Set the order method
     *
     */
    protected function orderByField($field, $values)
    {
        $orderings = array();
        foreach ($values as $value) {
            $orderings[$field . '=' . $value] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
        }

        return $orderings;
    }

}
