<?php
namespace RZ\Rzslick\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Create array
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class ToArrayViewHelper extends AbstractViewHelper
{

    /**
     * Return array
     *
     * @param string $string
     * @return array
     */
    public function render($string)
    {
        // Return array
        return explode(",", $string);
    }

}
