<?php
namespace RZ\Rzslick\ViewHelpers\Media;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\ViewHelpers\Media\Image\AbstractImageViewHelper;

/**
 * Renders an image tag for the given resource including all valid
 * HTML5 attributes. Derivates of the original image are rendered
 * if the provided (optional) dimensions differ.
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class ImageViewHelper extends AbstractImageViewHelper
{

    /**
     * name of the tag to be created by this view helper
     *
     * @var string
     * @api
     */
    protected $tagName = 'img';

    /**
     * Initialize arguments.
     *
     * @return void
     * @api
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('usemap', 'string', 'A hash-name reference to a map element with which to associate the image.', false);
        $this->registerTagAttribute('ismap', 'string', 'Specifies that its img element provides access to a server-side image map.', false, '');
        $this->registerTagAttribute('alt', 'string', 'Equivalent content for those who cannot process images or who have image loading disabled.', true);
    }

    /**
     * Render method
     *
     * @return string
     */
    public function render()
    {
        $this->preprocessImage();
        $src = $this->preprocessSourceUri($this->mediaSource);
        $this->tag->addAttribute('src', $src);

        if ('' === $this->arguments['title']) {
            $this->tag->addAttribute('title', $this->arguments['alt']);
        }
        return $this->tag->render();
    }

}
