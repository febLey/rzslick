<?php
namespace RZ\Rzslick\ViewHelpers\Content;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Render content
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class RenderViewHelper extends AbstractViewHelper
{

    /**
     * Render content
     *
     * @param int $uid
     * @return array
     */
    public function render($uid)
    {
        $lang = $GLOBALS['TSFE']->sys_language_uid;

        if ($lang == 0) {
            $field = 'uid';
        } else {
            $field = 't3_origuid';
        }

        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('uid', 'tt_content', $field . '=' . $uid . ' AND sys_language_uid=' . $lang);

        if ($row) {
            $conf = array(
                'tables' => 'tt_content',
                'source' => $row['uid'],
                'dontCheckPid' => 1,
            );

            return $GLOBALS['TSFE']->cObj->RECORDS($conf);
        }
    }

    /**
     * Debug
     *
     * @param string $var
     * @return array
     */
    protected function debug($var)
    {
        print_r("<pre>") . print_r($var) . print_r("</pre>");
    }

}
