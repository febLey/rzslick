<?php
namespace RZ\Rzslick\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\Asset;
use RZ\Rzslick\Utility\T3jquery;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Carousel controller
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class CarouselController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * List action
     *
     * @return void
     */
    public function listAction()
    {
        // Get uid
        $cObj = $this->configurationManager->getContentObject();
        $uid = $cObj->data['uid'];

        // Get images
        $images = $this->getFileReferences($uid);

        // Add JS?
        if (count($images) > 1) {
            // Add jQuery?
            $t3jqueryCheck = T3jquery::check();

            if ($t3jqueryCheck === false) {
                if ($this->settings['addJquery']) {
                    Asset::createFromSettings(array(
                        'name' => 'rzslick_jquery',
                        'path' => 'EXT:rzslick/Resources/Public/Js/jquery-1.12.3.min.js',
                    ));
                }
            }
        }

        // Set CE uid
        $this->view->assign('uid', $uid);

        // Set template vars
        $this->view->assign('images', $images);
        $this->view->assign('imageCount', count($images));
        $this->view->assign('settings', $this->settings);
    }

    /**
     * Get file references
     *
     * @param string $uid
     * @return array
     */
    protected function getFileReferences($uid)
    {
        $fileRepository = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\FileRepository');
        $fileObjects = $fileRepository->findByRelation('tt_content', 'rzslick.fe2.content02.images', $uid);

        $files = array();
        foreach ($fileObjects as $key => $value) {
            $files[$key]['reference'] = $value->getReferenceProperties();
            $files[$key]['original'] = $value->getOriginalFile()->getProperties();
        }

        return $files;
    }

    /**
     * Debug
     *
     * @param string $var
     * @return void
     */
    protected function debug($var)
    {
        print_r("<pre>") . print_r($var) . print_r("</pre>");
    }

}
